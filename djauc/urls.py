from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    (r'', include('djauc.base_app.urls', namespace='feature_app')),

    url(r'^admin/', include(admin.site.urls)),
)
