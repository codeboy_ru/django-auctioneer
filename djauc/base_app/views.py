# coding: utf-8

from datetime import datetime, timedelta
import json

from django.http import HttpResponse
from django.views.generic import View, TemplateView
from django.views.generic.base import TemplateResponseMixin
from django.core.exceptions import ObjectDoesNotExist

from djauc.mixins import JSONResponseMixin

from djauc.base_app.models import Lot


class Homepage(
    TemplateResponseMixin,
    View,
):
    """
    homepage
    """
    template_name = 'index.html'

    def get(self, request, *args, **kwargs):
        q_lots = Lot.objects.select_related().all()

        context = dict()
        context['lots'] = q_lots
        return Homepage.render_to_response(self, context)

