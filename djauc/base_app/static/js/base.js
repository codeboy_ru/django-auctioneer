/**
 * Created by codeboy on 10.12.2014.
 */


function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
var csrftoken = getCookie('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});



BaseApp = function () {
    this.params = {
//        'base': 'on'
    };

    this.init = function (params) {
        $.extend({}, this.params, params);
        var a = this;

        a.$send_btn = $('#send_btn');
        a.$result = $('div#result');


        a.starterLoadContent();
        a.bindEvents();

    };

    /**
     *   EVENTS & ACTIVATORS
     *******************************************************/
    this.bindEvents = function () {
        baseApp.$send_btn.on('click', baseApp.btnClick);
    };


    /********************************************
     *  FUNCTIONS
     *******************************************/

    this.starterLoadContent = function () {

        $.jsonRPC.setup({
            endPoint: '/api/',
            namespace: 'datagraph'
        });
    };


    /**
     * добавление машинок
     *************************************/
    this.btnClick = function () {

        var params = [
                {'car_id':baseApp.randId(), 'lon':baseApp.prepareLon(), 'lat':baseApp.prepareLat()},
                {'car_id':baseApp.randId(), 'lon':baseApp.prepareLon(), 'lat':baseApp.prepareLat()},
                {'car_id':baseApp.randId(), 'lon':baseApp.prepareLon(), 'lat':baseApp.prepareLat()}
            ];

        $.post( "/api/", {
            method: 'update_cars',
            cars: JSON.stringify(params)},
            'json'
        );

        var text = baseApp.$result.html();
        for (var i = 0; i < params.length; i++){
            console.log(params[i]['car_id']);
            text += 'добавлена машинка id:'+params[i]['car_id']+' lat:'+params[i]['lat']+' lon:'+params[i]['lon']+'<br/>'
        }
        baseApp.$result.html(text);

    };

    /**
     * рандомизатор id машин
     * @returns {number}
     */
    this.randId = function() {
        return baseApp.radomizerRange(1, 100);
    };

    /**
     * подготавливаем широту latitude
     * @returns {number}
     */
    this.prepareLat = function(){
        var rnum = baseApp.randLat();
        rnum = rnum.toString();
        rnum = parseFloat('5'+[rnum.slice(0, 1), '.', rnum.slice(1)].join(''));
        return rnum
    };

    /**
     * подготавливаем долготу longitude
     * @returns {number}
     */
    this.prepareLon = function(){
        var rnum = baseApp.randLon();
        rnum = rnum.toString();
        rnum = parseFloat('8'+[rnum.slice(0, 1), '.', rnum.slice(1)].join(''));
        return rnum
    };

    /**
     * рандомизатор lat
     * @returns {number}
     */
    this.randLat = function() {
        return baseApp.radomizerRange(492, 512);
    };

    /**
     * рандомизатор lon
     * @returns {number}
     */
    this.randLon = function() {
        return baseApp.radomizerRange(250, 310);
    };


    /**
     * рандомизатор в промежутке
     * @param min
     * @param max
     * @returns {number}
     */
    this.radomizerRange = function (min,max){
        return Math.floor(Math.random()*(max-min+1)+min);
    }

};


$(document).ready(function () {

    baseApp = new BaseApp();
    baseApp.init();

});