# coding: utf-8

import datetime

from django.db import models as m
from django.db import models as m
from django.template.defaultfilters import slugify
from django.contrib.auth.models import User

# from actstream import registry

# from django.utils.translation import ugettext_lazy as _
from djauc.utils import lang_stub as _



class Category(m.Model):
    name = m.CharField(max_length=245, verbose_name=_('name'))
    name_slug = m.SlugField(max_length=250, verbose_name=_('slug name'),)


# todo: add time lag before start - end
class Lot(m.Model):
    name = m.CharField(max_length=245, verbose_name=_('name'))
    name_slug = m.SlugField(max_length=250, verbose_name=_('slug name'),)
    description = m.TextField(_('description'))
    owner = m.ForeignKey(User)
    category = m.ForeignKey(Category, verbose_name=_('category'))

    datetime_created = m.DateTimeField(editable=False,
                                   default=datetime.datetime.now, verbose_name=_('creation date'),)
    datetime_modified = m.DateTimeField(editable=False,
                                    default=datetime.datetime.now, verbose_name=_('modified date'),)

    datetime_start = m.DateTimeField(default=datetime.datetime.now, verbose_name=_('datetime to start auction'),)
    datetime_end = m.DateTimeField(default=datetime.datetime.now, verbose_name=_('datetime to end auction'),)

    price_full = m.DecimalField(max_digits=14, decimal_places=5, default=0,
                                verbose_name=_('price of the lot'))
    price_start = m.DecimalField(max_digits=14, decimal_places=5, default=0,
                                 verbose_name=_('price to start auction'))
    price_end = m.DecimalField(max_digits=14, decimal_places=5, default=0,
                               verbose_name=_('price to end auction'))

    minimal_bid = m.DecimalField(max_digits=14, decimal_places=5, default=0,
                               verbose_name=_('minimal bid'))

    rating = m.PositiveSmallIntegerField(_('rating'), default=1)


    class Meta:
        verbose_name, verbose_name_plural = _(u'Lot'), _(u'Lots')

    def __unicode__(self):
        return self.name

    def save(self, *args, **kwargs):
        if self.name_slug is None:
            self.name_slug = slugify(self.name)
        self.datetime_modified = datetime.datetime.now()
        super(Lot, self).save(*args, **kwargs)


class Bid(m.Model):
    pass

# registry.register(User)