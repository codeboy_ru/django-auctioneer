# coding: utf-8

from django.contrib import admin

from models import Lot, Category


class DefaultAdmin(admin.ModelAdmin):
    save_on_top = True

class MixedSlugAdmin(DefaultAdmin):
    prepopulated_fields = {"name_slug": ("name",)}

admin.site.register(Lot, MixedSlugAdmin)
admin.site.register(Category, MixedSlugAdmin)


