# coding: utf-8

from django.conf.urls import url, patterns
from django.views.generic.base import TemplateView
from djauc.base_app import views as base_view

urlpatterns = patterns('',

    url(r'^$', base_view.Homepage.as_view(), name='hello'),

)
