# coding: utf-8

import json
from django.utils.unittest import TestCase
from django.test.client import Client

# from qa_projectile.features_app.models import Cars


class BaseTests(TestCase):
    def setUp(self):
        self.client = Client()

    def test_map(self):
        response = self.client.get(path='/')
        self.assertEqual(response.status_code, 200)

    def test_api(self):
        data = json.dumps([{'car_id':67,'lon':83.04,'lat':54.92}])
        response = self.client.post(path='/api/', data={'cars':data})
        self.assertEqual(response.status_code, 200)

    def test_cars_model(self):
        q_car = Cars()
        q_car.lon = 50.60
        q_car.lat = 60.50
        q_car.car_id = 22
        q_car.save()
        q_car_get = Cars.objects.get(id=q_car.pk)
        q_car_get.delete()

    def test_api_update(self):
        response = self.client.get('/map-update/')
        data = json.loads(response.content)
        self.assertEqual(type(data), type(list()))
        self.assertEqual(response.status_code, 200)

